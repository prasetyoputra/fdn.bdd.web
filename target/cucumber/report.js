$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("createAccount.feature");
formatter.feature({
  "line": 1,
  "name": "Create Account",
  "description": "",
  "id": "create-account",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Create account success and follow user that have badge",
  "description": "",
  "id": "create-account;create-account-success-and-follow-user-that-have-badge",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@QuickSmokeReg"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "i access website",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user clicks on the login portal button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user clicks create account button",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "the user should be directed to register page",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user enter the \"\u003cemail\u003e\" email register",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user enter the \"\u003cusername\u003e\" username register",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user enter the \"\u003cpassword\u003e\" password register",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user enter the \"\u003cpasswordagain\u003e\" confirm password register",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user check the agreement box",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user clicks on the create account button",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "the user should be directed to dob page",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "user choose the detail of dob",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "the user click next on dob",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "the user should be directed to skin type page",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "user choose the detail of skin type",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "the user click next on skin type",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "the user should be directed to skin tone page",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "user choose the detail of skin tone",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "the user click next on skin tone",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "the user should be directed to skin undertone page",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "user choose the detail of skin undertone",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "the user click next on skin undertone",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "the user should be directed to hair type page",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "user choose the detail of hair type",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "the user click next on hair type",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "the user should be directed to hair color page",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "user choose the detail of hair color",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "the user click next on hair color",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "the user should be directed to hijab page",
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "user choose the detail of hijab",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "the user click next on hijab",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "the user should be directed to skin concerns page",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "user choose the detail of skin concerns",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "the user click next on skin concerns",
  "keyword": "When "
});
formatter.step({
  "line": 47,
  "name": "the user should be directed to body concerns page",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "user choose the detail of body concerns",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "the user click next on body concerns",
  "keyword": "When "
});
formatter.step({
  "line": 51,
  "name": "the user should be directed to hair concerns page",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "user choose the detail of hair concerns",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "the user click next on hair concerns",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "the user should be directed to summary page",
  "keyword": "Then "
});
formatter.step({
  "line": 56,
  "name": "user fill all the detail of summary",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "the user click submit on summary",
  "keyword": "When "
});
formatter.step({
  "comments": [
    {
      "line": 58,
      "value": "#    Then the user should be directed to find friends page"
    }
  ],
  "line": 60,
  "name": "the user should be directed to homepage",
  "keyword": "Then "
});
formatter.examples({
  "line": 62,
  "name": "",
  "description": "",
  "id": "create-account;create-account-success-and-follow-user-that-have-badge;",
  "rows": [
    {
      "cells": [
        "email",
        "username",
        "password",
        "passwordagain"
      ],
      "line": 63,
      "id": "create-account;create-account-success-and-follow-user-that-have-badge;;1"
    },
    {
      "cells": [
        "cucumberpositive_a83@test.com",
        "cucumbera83",
        "test123",
        "test123"
      ],
      "line": 64,
      "id": "create-account;create-account-success-and-follow-user-that-have-badge;;2"
    }
  ],
  "keyword": "Examples"
});
});