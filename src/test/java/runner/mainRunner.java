package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.Test;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = {"src/test/java/feature/"},
        glue = {"stepDefinitions"},
        monochrome = true,
        tags = {"@QuickSmokeReg"},
        plugin = {"pretty","html:target/cucumber",
                "json:target/cucumber.json",
                "com.cucumber.listener.ExtentCucumberFormatter:target/report.html",
                "rerun:target/cucumber/rerun.txt",
                "usage:target/cucumber-usage.json"}
)

/*
        features = location feature file
        glue = location step file
        tags = specific test in feature and step file ("@tagsname")
        monochrome = true , console readable
        plugin = generate reports
 */

public class mainRunner extends AbstractTestNGCucumberTests {
}

