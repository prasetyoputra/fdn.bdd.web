Feature: Add Product

  @Positive
  Scenario Outline: Add Product
    Given i access website
    When user clicks on the login portal button
    And user enters the "<username>" username
    And user enter the "<password>" password
    When user clicks on the login button
    Then user should be directed to homepage
    When user click icon burger menu
    Then user click Reviews menu
    And user should be directed to reviews page
    When user hover add element and click add product
    And user should be directed to add product page
    Then user click yes im'sure when modal attention appear
    Then user click upload photo to add photo product
    Then user click next button on step 1
    And user select brand name
    And user select category
    And user select sub category
    And user fill "<productname>" in fied product name
    And user fill "<productshade>" in field product shade/varian
    Then user click next button on step 2
    And user choose overall rating
    And user choose how is the product price
    And user choose how is package quality
    And user choose would you repurchase this product
    And user input 200 char to write a review
    Then user click next button on step 3
    And user input the price
    And user input the product description
    And user input the product tags
    Then user click submit button on step 4
    And user will directed to review detail page

    Examples:
      |   username    |		password		 |    productname   | productshade  |
      |	  testqa	  |		test123 		 |    testname      | testshade     |


  @QuickSmokeAddProduct
  Scenario Outline: Add Product by insert valid image url
    Given i access website
    When user clicks on the login portal button
    And user enters the "<username>" username
    And user enter the "<password>" password
    When user clicks on the login button
    Then user should be directed to homepage
    When user click icon burger menu
    Then user click Reviews menu
    And user should be directed to reviews page
    When user hover add element and click add product
    And user should be directed to add product page
    Then user click yes im'sure when modal attention appear
    Then user paste url of images to insert images url box
    And user click show button
    Then user click next button on step 1
    And user select brand name
    And user select category
    And user select sub category
    And user fill "<productname>" in fied product name
    And user fill "<productshade>" in field product shade/varian
    Then user click next button on step 2
    And user choose overall rating
    And user choose how is the product price
    And user choose how is package quality
    And user choose would you repurchase this product
    And user input 200 char to write a review
    Then user click next button on step 3
    And user input the price
    And user input the product description
    And user input the product tags
    Then user click submit button on step 4
    And user will directed to review detail page

    Examples:
      |   username    |		password		 |    productname               | productshade              |
      |	  testqa	  |		test123 		 |    valid image prodname      | valid image prodshade     |