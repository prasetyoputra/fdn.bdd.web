Feature: Create Account

  @QuickSmokeReg
  Scenario Outline: Create account success and follow user that have badge
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then the user should be directed to dob page
    And user choose the detail of dob
    When the user click next on dob

    Then the user should be directed to skin type page
    And user choose the detail of skin type
    When the user click next on skin type

    Then the user should be directed to skin tone page
    And user choose the detail of skin tone
    When the user click next on skin tone

    Then the user should be directed to skin undertone page
    And user choose the detail of skin undertone
    When the user click next on skin undertone

    Then the user should be directed to hair type page
    And user choose the detail of hair type
    When the user click next on hair type

    Then the user should be directed to hair color page
    And user choose the detail of hair color
    When the user click next on hair color

    Then the user should be directed to hijab page
    And user choose the detail of hijab
    When the user click next on hijab

    Then the user should be directed to skin concerns page
    And user choose the detail of skin concerns
    When the user click next on skin concerns

    Then the user should be directed to body concerns page
    And user choose the detail of body concerns
    When the user click next on body concerns

    Then the user should be directed to hair concerns page
    And user choose the detail of hair concerns
    When the user click next on hair concerns

    Then the user should be directed to summary page
    And user fill all the detail of summary
    When the user click submit on summary
#    Then the user should be directed to find friends page

    Then the user should be directed to homepage

    Examples:
      | email                                |		 username		 |    password  |   passwordagain    |
      |	cucumberpositive_a83@test.com		 |		cucumbera83      |  test123     |   test123          |


  @Positive
  Scenario Outline: Create account success but skip detail concern
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then the user should be directed to dob page
    And user choose the detail of dob
    When the user click next on dob

    Then the user should be directed to skin type page
    And user choose the detail of skin type
    When the user click next on skin type

    Then the user should be directed to skin tone page
    And user choose the detail of skin tone
    When the user click next on skin tone

    Then the user should be directed to skin undertone page
    And user choose the detail of skin undertone
    When the user click next on skin undertone

    Then the user should be directed to hair color page
    And user choose the detail of hair color
    When the user click next on hair color

    Then the user should be directed to hijab page
    And user choose the detail of hijab
    When the user click next on hijab

    Then the user should be directed to skin concerns page
    And user choose to skip the detail of skin concerns
    When the user click next on skin concerns

    Then the user should be directed to body concerns page
    And user choose to skip the detail of body concerns
    When the user click next on body concerns

    Then the user should be directed to hair concerns page
    And user choose to skip the detail of hair concerns
    When the user click next on hair concerns

    Then the user should be directed to summary page
    And user fill all the detail of summary
    When the user click submit on summary
#    Then the user should be directed to find friends page

    Then the user should be directed to homepage

    Examples:
      | email                                |		 username		|    password  |   passwordagain    |
      |	cucumberpositive_b1@test.com		 |		cucumberb1      |  test123     |   test123          |


  @Positive
  Scenario Outline: Create account - skip upload photo and not fill location
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then the user should be directed to dob page
    And user choose the detail of dob
    When the user click next on dob

    Then the user should be directed to skin type page
    And user choose the detail of skin type
    When the user click next on skin type

    Then the user should be directed to skin tone page
    And user choose the detail of skin tone
    When the user click next on skin tone

    Then the user should be directed to skin undertone page
    And user choose the detail of skin undertone
    When the user click next on skin undertone

    Then the user should be directed to hair type page
    And user choose the detail of hair type
    When the user click next on hair type

    Then the user should be directed to hair color page
    And user choose the detail of hair color
    When the user click next on hair color

    Then the user should be directed to hijab page
    And user choose the detail of hijab
    When the user click next on hijab

    Then the user should be directed to skin concerns page
    And user choose the detail of skin concerns
    When the user click next on skin concerns

    Then the user should be directed to body concerns page
    And user choose the detail of body concerns
    When the user click next on body concerns

    Then the user should be directed to hair concerns page
    And user choose the detail of hair concerns
    When the user click next on hair concerns

    Then the user should be directed to summary page
    And user fill all the detail of summary except photo and location
    When the user click submit on summary
#    Then the user should be directed to find friends page

    Examples:
      | email                           |		 username		 |      password   |   passwordagain    |
      |	cucumberpositive_c1@test.com    |		cucumberc1 		 |      test123    |   test1234         |


  @Positive
  Scenario Outline: Create account after system display error message for submit register
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then system display error message that password doesnt match
    And user change and fill the "<correctpassword>" confirm password register
    When user clicks on the create account button
    Then the user should be directed to dob page

    Examples:
      | email                           |		 username		 |    password      |   correctpassword   |   passwordagain    |
      |	cucumberpositive_d1@test.com    |		cucumberd1 		 |     test123      |     test123         |   test1234         |


  @NegativeCase
  Scenario Outline: Create account failed because email already registered
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the registered "<registeredemail>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then system display error message that email already registed

    Examples:
      | registeredemail       |		 username	         |    password      |   passwordagain     |
      |	myjne001@gmail.com    |		cucumbere3 		 |     test123      |     test123         |


  @Negative
  Scenario Outline: Create account failed because username already registered
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the registered "<registereduser>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then system display error message that username already registered

    Examples:
      | email                           |       registereduser  |    password      |   passwordagain     |
      |	cucumbernegative_f1@test.com    |        testqa         |     test123      |     test123         |


  @Negative
  Scenario Outline: Create account failed because incorrect email format
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then system display error message that email is invalid format

    Examples:
      | email                                |		 username		 |    password    |   passwordagain    |
      |	cucumberpositive_g1@tesdasda		 |		cucumberg1       |    test123     |   test123          |


  @Negative
  Scenario Outline: Create account failed because username less than 3 char or more than 20 char
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then system display error message that username is less than 3 char or more than 20 char

    Examples:
      | email                                |		 username		                                      |    password    |   passwordagain    |
      |	cucumberpositive_h1@tesdasda		 |		usernamethanusernamethanusernamethanusernamethan      |    test123     |   test123          |


  @Negative
  Scenario Outline: Create account failed because password less than 6 char or more than 20 char
    Given i access website
    When user clicks on the login portal button
    Then user clicks create account button
    Then the user should be directed to register page
    And user enter the "<email>" email register
    And user enter the "<username>" username register
    And user enter the "<password>" password register
    And user enter the "<passwordagain>" confirm password register
    And user check the agreement box
    When user clicks on the create account button
    Then system display error message that username is less than 3 char or more than 20 char

    Examples:
      | email                                |		 username		|    password                                       |   passwordagain    |
      |	cucumberpositive_i1@tesdasda		 |		cucumberi1      |    test123test123test123test123test123test123     |   test123          |
