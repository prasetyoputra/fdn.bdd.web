Feature: Login into account

  @QuickSmoke
  Scenario Outline: Login to account with credentials
    Given i access website
    When user clicks on the login portal button
    And user enters the "<username>" username
    And user enter the "<password>" password
    When user clicks on the login button
    Then user should be directed to homepage


    Examples:
      |   username    |		password		 |
      |	  testqa	  |		test123 		 |
      |   putwid      |     tester123        |


    #Feature: Login into account
    #  Existing user should be able to login to account using correct credentials
    #
    #
    #Background:
    #  Given User navigates to stackoverflow website
    #  And User clicks on the login button on homepage
    #  And User enters a valid username
    #
    #  Scenario: Login into account with correct credentials
    #    And User enters   a "valid" password
    #    When User clicks on the login button
    #    Then User should be taken to the successful login page
    #
    #  Scenario: Login into account with correct credentials
    #    And User enters a "invalid" password
    #    When User clicks on the login button
    #    Then User should be taken to the successful login page

    #  \"([^\"]*)\"


#  waitAndClickElement
#  clickOnTextFromDropdownList
#  clickOnElementUsingCustomTimeout
#
#  actionMoveAndClick
#  actionMoveAndClickByLocator
#
#  sendKeysToWebElement
#
#  scrollToElementByWebElementLocator
#  jsPageScroll
#  waitAndclickElementUsingJS
#  jsClick
#
#  WaitUntilWebElementIsVisible
#  WaitUntilWebElementIsVisibleUsingByLocator
#  isElementClickable
#
#  closePopups
#  checkPopupIsVisible
#  isAlertPresent
#  closeAlertPopupBox