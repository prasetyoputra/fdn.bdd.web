Feature: Add Review

  @QuickSmoke
  Scenario Outline: Add review on product detail
    Given i access website
    When user clicks on the login portal button
    And user enters the "<username>" username
    And user enter the "<password>" password
    When user clicks on the login button
    Then user should be directed to homepage
    And user click skincare categories
    Then user directed to skincare landing page
    And user select menu night cream from category moisturizer
    Then user directed to product list page of night cream
    And user click one of product that never reviewed
    Then user directed to product detail page
    And user click add review button
    Then user fill in add review box that should contain than 200 char
    And user choose overall rating on product detail
    And user choose how is the product price on product detail
    And user choose how is package quality on product detail
    And user choose would you repurchase this product on product detail
    Then user click submit add review button
    And user will see the review that they submit on most top

    Examples:
      |   username      |		password		 |
      |	  testqa	    |		test123 		 |


  @QuickSmoke
  Scenario Outline: Add review select popular product
    Given i access website
    When user clicks on the login portal button
    And user enters the "<username>" username
    And user enter the "<password>" password
    When user clicks on the login button
    Then user should be directed to homepage
    When user click icon burger menu
    Then user click Reviews menu
    And user should be directed to reviews page
    When user hover add element and click add review
    And user should be directed to add review page
    When user click one of popular products
    Then user should directed to write a review tab
    Then user fill in add review box that should contain than 200 char
    And user choose overall rating on add review page
    And user choose how is the product price on add review page
    And user choose how is package quality on add review page
    And user choose would you repurchase this product on add review page
    Then user click submit review button
    Then user directed to product detail page
    And user will see the review that they submit on most top

    Examples:
      |   username      |		password		 |
      |	  testqa	    |		test123 		 |