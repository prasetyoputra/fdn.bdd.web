Feature: Add Talk

  @Positive
  Scenario Outline: Add Talk in new group
    Given i access website
    When user clicks on the login portal button
    And user enters the "<username>" username
    And user enter the "<password>" password
    When user clicks on the login button
    Then user should be directed to homepage
    When user click icon burger menu
    Then user click Talk menu
    Then user directed to talk homepage
    And user click group tab
    Then user directed to group list landing page
    And user click join new group
    And user click the group name
    Then user directed to group detail page
    And user will see member button
    When user click add topic button
    Then user directed to add topic form
    And user input title field
    And user input topic description field
    And user input topic tagging
    Then user click post button
    Then user directed to topic detail page
    When user click add talk button
    Then user directed to add talk form
    And user input talk description field


    Examples:
      |   username    |		password		 |    group         |
      |	  testqa	  |		test123 		 |    testname      |


  @Positive
  Scenario Outline: Add Topic in existing group
    Given i access website
    When user clicks on the login portal button
    And user enters the "<username>" username
    And user enter the "<password>" password
    When user clicks on the login button
    Then user should be directed to homepage
    When user click icon burger menu
    Then user click Talk menu
    Then user directed to talk homepage
    And user click existing group name
    Then user directed to group detail page
    And user will see member button
    When user click add topic button
    Then user directed to add topic form
    And user input title field
    And user input topic description field
    And user input topic tagging
    Then user click post button
    Then user directed to topic detail page
    When user click add talk button
    Then user directed to add talk form
    And user input talk description field

    Examples:
      |   username    |		password		 |    group         |
      |	  testqa	  |		test123 		 |    testname      |