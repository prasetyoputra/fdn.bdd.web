package stepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.driverFactory;

import java.util.concurrent.TimeUnit;

public class masterHooks extends driverFactory {

    @Before
    public void setup() {
        driver = getDriver();
    }

    @After
    public void tearDownAndScreenshotOnFailure(Scenario scenario) throws InterruptedException {
        try {
            if(driver != null && scenario.isFailed()) {
                scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "images/png");
//                driver.manage().deleteAllCookies();
//                driver.quit();
//                driver = null;
            }
            if(driver != null) {
//                driver.manage().deleteAllCookies();
//                driver.quit();
                driver = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("method failed" + e.getMessage());
        }
    }
}