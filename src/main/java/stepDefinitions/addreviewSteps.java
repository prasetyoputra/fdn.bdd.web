package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.basePage;
import utils.driverFactory;

public class addreviewSteps extends driverFactory {

    @And("^user click skincare categories$")
    public void user_click_skincare_categories() throws Throwable {
        homepage.clickSkincareCat();
    }

    @Then("^user directed to skincare landing page$")
    public void user_directed_to_skincare_landing_page() throws Throwable {
        categorylandingpage.getCurentUrlSkinLandingPage();
    }

    @And("^user select menu night cream from category moisturizer$")
    public void user_select_menu_night_cream_from_category_moisturizer() throws Throwable {
        categorylandingpage.clickNightCreamMenu();
    }

    @Then("^user directed to product list page of night cream$")
    public void user_directed_to_product_list_page_of_night_cream() throws Throwable {
        categorylandingpage.getCurentUrlProdListPage();
    }

    @And("^user click one of product that never reviewed$")
    public void user_click_one_of_product_that_never_reviewed() throws Throwable {
        categorylandingpage.clickFirstProduct();
    }

    @Then("^user directed to product detail page$")
    public void user_directed_to_product_detail_page() throws Throwable {
        categorylandingpage.getCurentUrlProdDetail();
    }

    @And("^user click add review button$")
    public void user_click_add_review_button() throws Throwable {
        productdetailpage.clickAddReviewButton();
    }

    @Then("^user fill in add review box that should contain than 200 char$")
    public void user_fill_in_add_review_box_that_should_contain_than_200_char() throws Throwable {
        productdetailpage.fillReviewDesc();
    }

    @And("^user choose overall rating on product detail$")
    public void user_choose_overall_rating_on_product_detail() throws Throwable {
        productdetailpage.chooseOverallRating();
    }

    @And("^user choose how is the product price on product detail$")
    public void user_choose_how_is_the_product_price_on_product_detail() throws Throwable {
        productdetailpage.chooseProductPrice();
    }

    @And("^user choose how is package quality on product detail$")
    public void user_choose_how_is_package_quality_on_product_detail() throws Throwable {
        productdetailpage.choosePackageQuality();
    }

    @And("^user choose would you repurchase this product on product detail$")
    public void user_choose_would_you_repurchase_this_product_on_product_detail() throws Throwable {
        productdetailpage.chooseRepurchase();
    }

    @Then("^user click submit add review button$")
    public void user_click_submit_add_review_button() throws Throwable {
        productdetailpage.clickSubmitReviewButton();
    }

    @And("^user will see the review that they submit on most top$")
    public void user_will_see_the_review_that_they_submit_on_most_top() throws Throwable {
        productdetailpage.checkReviewOnMostTop();
    }

    // add review from own page

    @When("^user hover add element and click add review$")
    public void user_hover_add_element_and_click_add_review() throws Throwable {
        reviewspage.hoverClickAddReviewElement();
    }

    @And("^user should be directed to add review page$")
    public void user_should_be_directed_to_add_review_page() throws Throwable {
        reviewspage.getCurrentUrlAddReviewPage();
    }

    @When("^user click one of popular products$")
    public void user_click_one_of_popular_products() throws Throwable {
        reviewspage.clickOneOfPopularProducts();
    }

    @Then("^user should directed to write a review tab$")
    public void user_should_directed_to_write_a_review_tab() throws Throwable {
       reviewspage.checkIfUserOnReviewTab();
    }

    @And("^user choose overall rating on add review page$")
    public void user_choose_overall_rating_on_add_review_page() throws Throwable {
        reviewspage.chooseOverallRating();
    }

    @And("^user choose how is the product price on add review page$")
    public void user_choose_how_is_the_product_price_on_add_review_page() throws Throwable {
        reviewspage.chooseProductPrice();
    }

    @And("^user choose how is package quality on add review page$")
    public void user_choose_how_is_package_quality_on_add_review_page() throws Throwable {
        reviewspage.choosePackageQuality();
    }

    @And("^user choose would you repurchase this product on add review page$")
    public void user_choose_would_you_repurchase_this_product_on_add_review_page() throws Throwable {
        reviewspage.chooseRepurchaseYes();
    }

    @Then("^user click submit review button$")
    public void user_click_submit_review_button() throws Throwable {
        reviewspage.clickSubmitReview();
    }

}
