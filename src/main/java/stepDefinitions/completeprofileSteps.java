package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.basePage;
import utils.driverFactory;

public class completeprofileSteps extends driverFactory {

    @Then("^the user should be directed to dob page$")
    public void the_user_should_be_directed_to_dob_page() throws Throwable {
        completeprofilepage.getCurentUrlDob();
    }

    //dob
    @And("^user choose the detail of dob$")
    public void user_choose_the_detail_of_dob() throws Throwable {

        //step fill dob
        completeprofilepage.clickDataMonth();
        completeprofilepage.getDataMonth("January");

        completeprofilepage.clickBtnNextDob();
    }

    @Then("^the user click next on dob$")
    public void the_user_click_next_on_dob() throws Throwable {
        completeprofilepage.clickBtnNextDob();
        completeprofilepage.getCurentUrlSkinType();
    }

    //skin type
    @Then("^the user should be directed to skin type page$")
    public void the_user_should_be_directed_to_skin_type_page() throws Throwable {
        completeprofilepage.getCurentUrlSkinType();
    }

    @And("^user choose the detail of skin type$")
    public void user_choose_the_detail_of_skin_type() throws Throwable {
        completeprofilepage.clickSkinType_Dry();
    }

    @When("^the user click next on skin type$")
    public void the_user_click_next_on_skin_type() throws Throwable {
        completeprofilepage.clickBtnNextSkinType();
    }

    //skin tone
    @Then("^the user should be directed to skin tone page$")
    public void the_user_should_be_directed_to_skin_tone_page() throws Throwable {
        completeprofilepage.getCurentUrlSkinTone();
    }

    @And("^user choose the detail of skin tone$")
    public void user_choose_the_detail_of_skin_tone() throws Throwable {
        completeprofilepage.clickSkinTone_Medium();
    }

    @When("^the user click next on skin tone$")
    public void the_user_click_next_on_skin_tone() throws Throwable {
        completeprofilepage.clickBtnNextSkinTone();
    }

    //skin_undertone
    @Then("^the user should be directed to skin undertone page$")
    public void the_user_should_be_directed_to_skin_undertone_page() throws Throwable {
        completeprofilepage.getCurentUrlSkinUnderTone();
    }

    @And("^user choose the detail of skin undertone$")
    public void user_choose_the_detail_of_skin_undertone() throws Throwable {
        completeprofilepage.clickSkinUnderTone_Warm();
    }

    @When("^the user click next on skin undertone$")
    public void the_user_click_next_on_skin_undertone() throws Throwable {
        completeprofilepage.clickBtnNextSkinUnderTone();
    }

    //hair_type
    @Then("^the user should be directed to hair type page$")
    public void the_user_should_be_directed_to_hair_type_page() throws Throwable {
        completeprofilepage.getCurentUrlHairType();
    }

    @And("^user choose the detail of hair type$")
    public void user_choose_the_detail_of_hair_type() throws Throwable {
        completeprofilepage.clickHairType_Curly();
    }

    @When("^the user click next on hair type$")
    public void the_user_click_next_on_hair_type() throws Throwable {
        completeprofilepage.clickBtnNextHairType();
    }

    //hair_color
    @Then("^the user should be directed to hair color page$")
    public void the_user_should_be_directed_to_hair_color_page() throws Throwable {
        completeprofilepage.getCurentUrlHairColor();
    }

    @And("^user choose the detail of hair color$")
    public void user_choose_the_detail_of_hair_color() throws Throwable {
        completeprofilepage.clickHairColor_Yes();
    }

    @When("^the user click next on hair color$")
    public void the_user_click_next_on_hair_color() throws Throwable {
        completeprofilepage.clickBtnNextHairColor();
    }

    //hijab
    @Then("^the user should be directed to hijab page$")
    public void the_user_should_be_directed_to_hijab_page() throws Throwable {
        completeprofilepage.getCurentUrlHijab();
    }

    @And("^user choose the detail of hijab$")
    public void user_choose_the_detail_of_hijab() throws Throwable {
        completeprofilepage.clickHijaber_Yes();
    }

    @When("^the user click next on hijab$")
    public void the_user_click_next_on_hijab() throws Throwable {
        completeprofilepage.clickBtnNextHijab();
    }

    //skin_concerns
    @Then("^the user should be directed to skin concerns page$")
    public void the_user_should_be_directed_to_skin_concerns_page() throws Throwable {
        completeprofilepage.getCurentUrlSkinCon();
    }

    @And("^user choose the detail of skin concerns$")
    public void user_choose_the_detail_of_skin_concerns() throws Throwable {
        completeprofilepage.clicSkinCon_Blackheads();
    }

    @When("^the user click next on skin concerns$")
    public void the_user_click_next_on_skin_concerns() throws Throwable {
        completeprofilepage.clickBtnNextSkinCon();
    }

    //body_concerns
    @Then("^the user should be directed to body concerns page$")
    public void the_user_should_be_directed_to_body_concerns_page() throws Throwable {
        completeprofilepage.getCurentUrlBodyCon();
    }

    @And("^user choose the detail of body concerns$")
    public void user_choose_the_detail_of_body_concerns() throws Throwable {
        completeprofilepage.clickBodyCon_Dryness();
    }

    @When("^the user click next on body concerns$")
    public void the_user_click_next_on_body_concerns() throws Throwable {
        completeprofilepage.clickBtnNextBodyCon();
    }

    //hair_concerns
    @Then("^the user should be directed to hair concerns page$")
    public void the_user_should_be_directed_to_hair_concerns_page() throws Throwable {
        completeprofilepage.getCurentUrlHairCon();
    }

    @And("^user choose the detail of hair concerns$")
    public void user_choose_the_detail_of_hair_concerns() throws Throwable {
        completeprofilepage.clickHairCon_Dryness();
    }

    @When("^the user click next on hair concerns$")
    public void the_user_click_next_on_hair_concerns() throws Throwable {
        completeprofilepage.clickBtnNextHairCon();
    }

    //skip concern
    @And("^user choose to skip the detail of skin concerns$")
    public void user_choose_to_skip_the_detail_of_skin_concerns() throws Throwable {
        completeprofilepage.clickFillThisLaterSkinCon();
    }

    @And("^user choose to skip the detail of body concerns$")
    public void user_choose_to_skip_the_detail_of_body_concerns() throws Throwable {
        completeprofilepage.clickFillThisLaterBodyCon();
    }

    @And("^user choose to skip the detail of hair concerns$")
    public void user_choose_to_skip_the_detail_of_hair_concerns() throws Throwable {
        completeprofilepage.clickFillThisLaterHairCon();
    }

    //summary

    @Then("^the user should be directed to summary page$")
    public void the_user_should_be_directed_to_summary_page() throws Throwable {
        completeprofilepage.getCurentUrlSummary();
    }

    @And("^user fill all the detail of summary$")
    public void user_fill_all_the_detail_of_summary() throws Throwable {
        completeprofilepage.fillFullname();
        completeprofilepage.fillNoTelephone();
        completeprofilepage.chooseLocation();

        completeprofilepage.fillBio();
        completeprofilepage.chooseFavBrand();
    }

    @And("^user fill all the detail of summary except photo and location$")
    public void user_fill_all_the_detail_of_summary_except_photo_and_location() throws Throwable {
        completeprofilepage.fillFullname();
        completeprofilepage.fillNoTelephone();

        completeprofilepage.fillBio();
        completeprofilepage.chooseFavBrand();
    }

    @When("^the user click submit on summary$")
    public void the_user_click_submit_on_summary() throws Throwable {
        completeprofilepage.clickFinishBtn();
    }

    @Then("^the user should be directed to find friends page$")
    public void the_user_should_be_directed_to_find_friends_page() throws Throwable {
        completeprofilepage.getCurentUrlFindFriends();
    }

    @Then("^the user should be directed to homepage$")
    public void the_user_should_be_directed_to_homepage() throws Throwable {
        homepage.checkDirectHomepage();
    }
}
