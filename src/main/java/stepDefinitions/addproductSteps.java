package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.basePage;
import utils.driverFactory;

public class addproductSteps extends driverFactory {

    @When("^user click icon burger menu$")
    public void user_click_icon_burger_menu() throws Throwable {
        homepage.clickOnHamburgerMenu();
    }

    @Then("^user click Reviews menu$")
    public void user_click_reviews_menu() throws Throwable {
        homepage.clickMenuReviews();
    }

    @And("^user should be directed to reviews page$")
    public void user_should_be_directed_to_reviews_page() throws Throwable {
        reviewspage.getCurentUrlReviews();
    }

    @When("^user hover add element and click add product$")
    public void user_hover_add_element_and_click_add_product() throws Throwable {
        reviewspage.hoverClickAddProductElement();
    }

    @And("^user should be directed to add product page$")
    public void user_should_be_directed_to_add_product_page() throws Throwable {
        addproductpage.getCurentUrlAddProduct();
    }

    @Then("^user click yes im'sure when modal attention appear$")
    public void user_click_yes_imsure_when_modal_attention_appear() throws Throwable {
        addproductpage.clickImSure();
    }

    @Then("^user click upload photo to add photo product$")
    public void user_click_upload_photo_to_add_photo_product() throws Throwable {
       addproductpage.clickUploadPhoto();
    }

    @Then("^user click next button on step 1$")
    public void user_click_next_button_on_step_1() throws Throwable {
        addproductpage.clickNextStep1();
    }

    @And("^user select brand name$")
    public void user_select_brand_name() throws Throwable {
        addproductpage.selectBrandName();
    }

    @And("^user select category$")
    public void user_select_category() throws Throwable {
        addproductpage.selectCategoryName();
    }

    @And("^user select sub category$")
    public void user_select_sub_category() throws Throwable {
        addproductpage.selectSubCategoryName();
    }

    @And("^user fill \"([^\"]*)\" in fied product name$")
    public void user_fill_something_in_fied_product_name(String productname) throws Throwable {
        addproductpage.fillProductName(productname);
    }

    @And("^user fill \"([^\"]*)\" in field product shade/varian$")
    public void user_fill_something_in_field_product_shadevarian(String productshade) throws Throwable {
        addproductpage.fillProductShade(productshade);
    }

    @Then("^user click next button on step 2$")
    public void user_click_next_button_on_step_2() throws Throwable {
        addproductpage.clickNextStep2();
    }

    @And("^user choose overall rating$")
    public void user_choose_overall_rating() throws Throwable {
        addproductpage.chooseOverallRating();
    }

    @And("^user choose how is the product price$")
    public void user_choose_how_is_the_product_price() throws Throwable {
        addproductpage.chooseProductPrice();
    }

    @And("^user choose how is package quality$")
    public void user_choose_how_is_package_quality() throws Throwable {
        addproductpage.choosePackageQuality();
    }

    @And("^user choose would you repurchase this product$")
    public void user_choose_would_you_repurchase_this_product() throws Throwable {
        addproductpage.chooseRepurchase();
    }

    @And("^user input 200 char to write a review$")
    public void user_input_200_char_to_write_a_review() throws Throwable {
        addproductpage.fillWriteReview();
    }

    @Then("^user click next button on step 3$")
    public void user_click_next_button_on_step_3() throws Throwable {
        addproductpage.clickNextStep3();
    }

    @And("^user input the price$")
    public void user_input_the_price() throws Throwable {
        addproductpage.fillRetailPrice();
    }

    @And("^user input the product description$")
    public void user_input_the_product_description() throws Throwable {
        addproductpage.fillProductDesc();
    }

    @And("^user input the product tags$")
    public void user_input_the_product_tags() throws Throwable {
        addproductpage.fillProductTags();
    }

    @Then("^user click submit button on step 4$")
    public void user_click_submit_button_on_step_4() throws Throwable {
        addproductpage.clickSubmit();
    }

    @And("^user will directed to review detail page$")
    public void user_will_directed_to_review_detail_page() throws Throwable {
       addproductpage.getCurentUrlSubmittedProduct();
    }

    @Then("^user paste url of images to insert images url box$")
    public void user_paste_url_of_images_to_insert_images_url_box() throws Throwable {
        addproductpage.pasteUrlImages();
    }

    @And("^user click show button$")
    public void user_click_show_button() throws Throwable {
        addproductpage.clickShowUrlImages();
    }



}
