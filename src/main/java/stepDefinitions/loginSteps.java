package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import pageObjects.basePage;
import pageObjects.loginPage;
import utils.driverFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class loginSteps extends driverFactory {

    @Given("^i access website$")
    public void i_access_website() throws Throwable {
        loginpage.getUrlWebsite();
    }

    @When("^user clicks on the login portal button$")
    public void user_clicks_on_the_login_portal_button() throws Throwable {
        homepage.clickOnLoginHeader();
    }

    @And("^user enters the \"([^\"]*)\" username$")
    public void user_enters_the_username(String username) throws Throwable {
       loginpage.inputEmailUsername(username);
    }

    @And("^user enter the \"([^\"]*)\" password$")
    public void user_enter_the_password(String password) throws Throwable {
        loginpage.inputPassword(password);
    }

    @When("^user clicks on the login button$")
    public void user_clicks_on_the_login_button() throws Throwable {
        loginpage.clickButtonLogin();
    }

    @Then("^user should be directed to homepage$")
    public void user_should_be_directed_to_homepage() throws Throwable {
        homepage.checkLoggedIn();
    }
}
