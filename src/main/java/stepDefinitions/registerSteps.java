package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.registerPage;
import utils.driverFactory;

public class registerSteps extends driverFactory {

//    @Given("^i access website$")
//    public void i_access_website() throws Throwable {
//        loginpage.getUrlWebsite();
//    }

//    @When("^user clicks on the login portal button$")
//    public void user_clicks_on_the_login_portal_button() throws Throwable {
//        homepage.clickOnLoginHeader();
//    }

    @When("^user clicks on the create account button$")
    public void user_clicks_on_the_create_account_button() throws Throwable {
        loginpage.clickButtonCreateAccount();
    }

    @Then("^user clicks create account button$")
    public void user_clicks_create_account_button() throws Throwable {
        registerpage.clickButtonCreateAccount();
    }

    @Then("^the user should be directed to register page$")
    public void the_user_should_be_directed_to_register_page() throws Throwable {
        registerpage.waitregisterpageload();
    }

    @And("^user enter the \"([^\"]*)\" email register$")
    public void user_enter_the_email_register(String email) throws Throwable {
        registerpage.inputEmail(email);
    }

    @And("^user enter the registered \"([^\"]*)\" email register$")
    public void user_enter_the_registered_something_email_register(String registeredemail) throws Throwable {
        registerpage.inputEmail(registeredemail);
    }

    @And("^user enter the \"([^\"]*)\" username register$")
    public void user_enter_the_username_register(String username) throws Throwable {
        registerpage.inputUsername(username);
    }

    @And("^user enter the registered \"([^\"]*)\" username register$")
    public void user_enter_the_registered_something_username_register(String registereduser) throws Throwable {
        registerpage.inputUsername(registereduser);
    }

    @And("^user enter the \"([^\"]*)\" password register$")
    public void user_enter_the_password_register(String password) throws Throwable {
        registerpage.inputPassword(password);
    }

    @And("^user enter the \"([^\"]*)\" confirm password register$")
    public void user_enter_the_confirm_password(String passwordagain) throws Throwable {
        registerpage.inputConfirmPassword(passwordagain);
    }

    @And("^user change and fill the \"([^\"]*)\" confirm password register$")
    public void user_change_and_fill_the_something_confirm_password_register(String correctpassword) throws Throwable {
        registerpage.inputConfirmPassword(correctpassword);
    }

    @And("^user check the agreement box$")
    public void user_check_the_agreement_box() throws Throwable {
        registerpage.checkboxAgree();
    }

    /* error message */

    @Then("^system display error message that email already registed$")
    public void system_display_error_message_that_email_already_registered() throws Throwable {
        registerpage.getMsgEmailRegistered();
    }

    @Then("^system display error message that password doesnt match$")
    public void system_display_error_message_that_password_doesnt_match() throws Throwable {
        registerpage.getMsgPasswordDoesntMatch();
    }

    @Then("^system display error message that username already registered$")
    public void system_display_error_message_that_username_already_registered() throws Throwable {
        registerpage.getMsgUsernameRegistered();
    }

    @Then("^system display error message that email is invalid format$")
    public void system_display_error_message_that_email_is_invalid_format() throws Throwable {
        registerpage.getMsgInvFormatEmail();
    }

    @Then("^system display error message that username is less than 3 char or more than 20 char$")
    public void system_display_error_message_that_username_is_less_than_3_char_or_more_than_20_char() throws Throwable {
        registerpage.getMsgUsernamelessOrThan();
    }

}
