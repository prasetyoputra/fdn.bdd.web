package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import pageObjects.loginPage;
import pageObjects.homePage;
import pageObjects.registerPage;
import pageObjects.completeprofilePage;
import pageObjects.addproductPage;
import pageObjects.reviewsPage;
import pageObjects.categorylandingPage;
import pageObjects.productdetailPage;

import pageObjects.groupdetailPage;
import java.pageObjects.grouplisttalkPage;
import java.pageObjects.topicdetailPage;
import java.pageObjects.talkhomePage;
import utils.constant;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.pageObjects.groupdetailPage;
import java.pageObjects.grouplisttalkPage;
import java.pageObjects.talkhomePage;
import java.pageObjects.topicdetailPage;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class driverFactory {

    //grid
    public static RemoteWebDriver driver;

    public static final String URL_HUB = "http://selenium__standalone-chrome:4444/wd/hub";

    //local
    //public static WebDriver driver;

    //page object package
    public static loginPage loginpage;
    public static homePage homepage;
    public static registerPage registerpage;
    public static completeprofilePage completeprofilepage;
    public static addproductPage addproductpage;
    public static reviewsPage reviewspage;
    public static categorylandingPage categorylandingpage;
    public static productdetailPage productdetailpage;

    //root cause
    public static groupdetailPage groupdetailpage;
//    public static grouplisttalkPage grouplisttalkpage;
//    public static topicdetailPage topicdetailpage;
//    public static talkhomePage talkhomepage;

    public static Properties prop;
    private static String cwd = System.getProperty("user.dir");

    public String readUrl() throws IOException {
        prop = new Properties();
        FileInputStream ip = new FileInputStream(cwd + "//src//main//java//properties//config.properties");
        prop.load(ip);
        String url = prop.getProperty("env");
        return url;
    }

    public RemoteWebDriver getDriver() {
        try {
            // Read Config
            utils.readConfigFile file = new utils.readConfigFile();
            String browserName = file.getBrowser();
            String url = file.getUrl();

            switch (browserName) {

                case "firefox":
                    // code
                    if (null == driver) {
                        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                        System.setProperty("webdriver.gecko.driver", utils.constant.GECKO_DRIVER_DIRECTORY);
                        capabilities.setCapability("marionette", true);
                        driver = new FirefoxDriver();
                    }
                    break;

                case "chrome":
                    // code
                    if (null == driver) {
                        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                        capabilities.setCapability(ChromeOptions.CAPABILITY, utils.constant.CHROME_DRIVER_DIRECTORY);
                        driver = new RemoteWebDriver(new URL("http://10.10.10.241:5555/wd/hub"), capabilities);
//                        driver = new RemoteWebDriver(new URL(URL_HUB), capabilities);
                    }
                        break;
            }
        } catch (Exception e) {
            System.out.println("Unable to load browser: " + e.getMessage());
        } finally {
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            loginpage = PageFactory.initElements(driver, loginPage.class);
            homepage = PageFactory.initElements(driver, homePage.class);
            registerpage = PageFactory.initElements(driver, registerPage.class);
            completeprofilepage = PageFactory.initElements(driver, completeprofilePage.class);
            addproductpage = PageFactory.initElements(driver, addproductPage.class);
            reviewspage = PageFactory.initElements(driver, reviewsPage.class);
            categorylandingpage = PageFactory.initElements(driver, categorylandingPage.class);
            productdetailpage = PageFactory.initElements(driver, productdetailPage.class);

            groupdetailpage = PageFactory.initElements(driver, groupdetailPage.class);
//            grouplisttalkpage = PageFactory.initElements(driver, grouplisttalkPage.class);
//            topicdetailpage = PageFactory.initElements(driver, topicdetailPage.class);
//            talkhomepage = PageFactory.initElements(driver, talkhomePage.class);
        }
        return driver;
    }
}
