package utils;

public class constant {

    private static String cwd = System.getProperty("user.dir");

    /**Config Properties file **/
    public final static String CONFIG_PROPERTIES_DIRECTORY = cwd+"//src//main//java//properties//config.properties";

    public final static String GECKO_DRIVER_DIRECTORY = System.getProperty("user.dir") + "//geckodriver";

    public final static String CHROME_DRIVER_DIRECTORY = System.getProperty("user.dir") + "//chromedriver";
}
