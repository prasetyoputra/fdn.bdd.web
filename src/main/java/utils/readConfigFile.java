package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class readConfigFile {

    protected InputStream input = null;
    protected Properties prop = null;

    private static String cwd = System.getProperty("user.dir");

    public readConfigFile() {
        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(cwd+"//src//main//java//properties//config.properties");
            prop.load(ip);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getBrowser() {
        if (prop.getProperty("browser") == null)
            return "";
        return prop.getProperty("browser");
    }

    public String getUrl() {
        if (prop.getProperty("env") == null)
            return "";
        return prop.getProperty("env");
    }
}
