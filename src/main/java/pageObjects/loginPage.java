package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import pageObjects.basePage;


public class loginPage extends basePage {

    public @FindBy(css = "#id-email-username") WebElement useremailField;
    public @FindBy(css = "#id-password") WebElement passwordField;
    public @FindBy(css = "div[data-element-id='taste-checkbox-container']") WebElement remembermeBox;

    public @FindBy(css = "input[value='Login']") WebElement loginBtn;
    public @FindBy(css = "input[value='Login with Facebook']") WebElement loginfbBtn;
    public @FindBy(linkText = "Forgot Password?") WebElement forgotpassLink;
    public @FindBy(xpath = "//*[@id=\'__next\']/div/div/div[2]/div/form/div[7]/div/input") WebElement createaccountBtn;

    public loginPage() throws IOException {
        super();
    }

    public loginPage getUrlWebsite() throws IOException {
        basePage bp = new basePage();
        getDriver().get(bp.readUrl());
        return new loginPage();
    }

    public loginPage inputEmailUsername(String nama) throws Exception {
        sendKeysToWebElement(useremailField, nama, Keys.ENTER);
        return new loginPage();
    }

    public loginPage inputPassword(String password) throws Exception {
        sendKeysToWebElement(passwordField, password, Keys.ENTER);
        return new loginPage();
    }

    public loginPage clickButtonLogin() throws Exception {
        Thread.sleep(1000);
        waitAndClickElement(loginBtn);
        return new loginPage();
    }

    public loginPage clickForgotPassword() throws Exception {
        waitAndClickElement(forgotpassLink);
        return new loginPage();
    }

    public loginPage clickButtonFb() throws Exception {
        waitAndClickElement(loginfbBtn);
        return new loginPage();
    }

    public loginPage clickButtonCreateAccount() throws Exception {
        waitAndClickElement(createaccountBtn);
        return new loginPage();
    }

    public loginPage uncheckBoxRemember() throws Exception {
        waitAndClickElement(remembermeBox);
        return new loginPage();
    }
}
