package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import pageObjects.basePage;

public class completeprofilePage extends basePage {

    public @FindBy(xpath ="//*[@id=\'birthday\']/div[2]/div/div[1]") WebElement dobMonth;
    public @FindBy(xpath ="//*[@id=\'birthday\']/div[2]/div/div[2]") WebElement dobDate;
    public @FindBy(xpath ="//*[@id=\'birthday\']/div[2]/div/div[3]") WebElement dobYear;

    public @FindBy(xpath ="//*[@id=\'birthday\']/div[2]/div/div[1]") WebElement fieldDobMonth;
    public @FindBy(xpath ="//*[@id=\'react-select-1824--value-item\']") WebElement fieldDobDate;
    public @FindBy(xpath ="//*[@id=\'react-select-1825--value-item\']") WebElement fieldDobYear;

    public @FindBy(xpath ="//*[@id=\'birthday\']/div[2]/button[2]") WebElement btnNextDob;
    public @FindBy(xpath ="//*[@id=\'skin-type\']/div[2]/button[2]") WebElement btnNextSkinType;
    public @FindBy(xpath ="//*[@id=\'skin-tone\']/div[2]/button[2]") WebElement btnNextSkinTone;
    public @FindBy(xpath ="//*[@id=\'skin-undertone\']/div[2]/button[2]") WebElement btnNextSkinUnderTone;
    public @FindBy(xpath ="//*[@id=\'hair-type\']/div[2]/button[2]") WebElement btnNextHairType;
    public @FindBy(xpath ="//*[@id=\'colored-hair\']/div[2]/button[2]") WebElement btnNextHairColor;
    public @FindBy(xpath ="//*[@id=\'hijaber\']/div[2]/button[2]") WebElement btnNextHijab;
    public @FindBy(xpath ="//*[@id=\'skin-concern\']/div[2]/button[2]") WebElement btnNextSkinCon;
    public @FindBy(xpath ="//*[@id=\'body-concern\']/div[2]/button[2]") WebElement btnNextBodyCon;
    public @FindBy(xpath ="//*[@id=\'hair-concern\']/div[2]/button[2]") WebElement btnNextHairCon;

    By skintype_dry=By.cssSelector("div[data-value='3']");
    By skintone_medium=By.cssSelector("div[data-value='3']");
    By skinundertone_warm=By.cssSelector("div[data-value='3']");
    By hairtype_curly=By.cssSelector("div[data-value='3']");
    By haircolor_yes=By.cssSelector("div[data-value='1']");
    By hijab_yes=By.cssSelector("div[data-value='1']");
    By skincon_bwheads=By.cssSelector("div[data-value='14']");
    By bodycon_dryness=By.cssSelector("div[data-value='3']");
    By haircon_dryness=By.cssSelector("div[data-value='3']");

    By fillthislaterSkinCon=By.cssSelector("a[id='id-fill-later']");
    By fillthislaterBodyCon=By.cssSelector("a[id='id-fill-later']");
    By fillthislaterHairCon=By.cssSelector("a[id='id-fill-later']");

    public @FindBy(xpath ="//span[@class='jsx-1743604549 icon-btn_add_photo fs30']") WebElement addPhoto;
    public @FindBy(xpath ="//div[@class='jsx-1743604549 summary-name']//input[@id='taste-input']") WebElement fieldFullname;
    public @FindBy(xpath ="//div[@class='jsx-1743604549 summary-phone']//input[@id='taste-input']") WebElement fieldPhoneNo;
    public @FindBy(xpath ="//div[@class='jsx-1743604549 summary-textarea']") WebElement fieldBio;
    public @FindBy(xpath ="//div[contains(text(),'Location')]") WebElement optionCity;
    public @FindBy(xpath ="//div[@id='#react-select-2--list']") WebElement listCity;
    public @FindBy(xpath ="//div[@id='react-select-2--option-3']']") WebElement selectOptionCity;
    public @FindBy(xpath ="//div[@class='jsx-1743604549 summary-favourite-brands']") WebElement optionFavBrand;
    public @FindBy(xpath ="//div[@class='Select-menu-outer']") WebElement listFavBrand;
    public @FindBy(xpath ="//input[@value='Finish']") WebElement btnFinish;

    public completeprofilePage() throws IOException {
        super();
    }

    // get current url start
    public completeprofilePage getCurentUrlDob() throws IOException {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=birthdate"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextDob() throws Exception {
        waitAndClickElement(btnNextDob);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlSkinType() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=skin_type"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextSkinType() throws Exception {
        waitAndClickElement(btnNextSkinType);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlSkinTone() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=skin_tone"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextSkinTone() throws Exception {
        waitAndClickElement(btnNextSkinTone);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlSkinUnderTone() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=skin_undertone"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextSkinUnderTone() throws Exception {
        waitAndClickElement(btnNextSkinUnderTone);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlHairType() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=hair_type"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextHairType() throws Exception {
        waitAndClickElement(btnNextHairType);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlHairColor() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=hair_color"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextHairColor() throws Exception {
        waitAndClickElement(btnNextHairColor);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlHijab() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=hijab"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextHijab() throws Exception {
        waitAndClickElement(btnNextHijab);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlSkinCon() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=skin_concerns"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextSkinCon() throws Exception {
        waitAndClickElement(btnNextSkinCon);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlBodyCon() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=body_concerns"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextBodyCon() throws Exception {
        waitAndClickElement(btnNextBodyCon);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlHairCon() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=hair_concerns"));
        return new completeprofilePage();
    }

    public completeprofilePage clickBtnNextHairCon() throws Exception {
        waitAndClickElement(btnNextHairCon);
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlSummary() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/completing-profile?step=summary"));
        return new completeprofilePage();
    }

    public completeprofilePage getCurentUrlFindFriends() throws Exception {

        driver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);

        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("/completing-profile?step=find_friends"));
        return new completeprofilePage();
    }
    // get current url finish

    //dob
    public completeprofilePage getDataMonth(String bulan) throws IOException {
        String dobsummonth = fieldDobMonth.getText();
        assertTrue(dobsummonth.contains(bulan));
        return new completeprofilePage();
    }

    public completeprofilePage getDataDate(String tanggal) throws IOException {
        String dobsumdate = fieldDobDate.getText();
        assertEquals(dobsumdate,tanggal);
        return new completeprofilePage();
    }

    public completeprofilePage getDataYear(String tahun) throws IOException {
        String dobsumyear = fieldDobYear.getText();
        assertEquals(dobsumyear,tahun);
        return new completeprofilePage();
    }

    public completeprofilePage clickDataMonth() throws IOException, InterruptedException {
        waitAndClickElement(dobMonth);
        return new completeprofilePage();
    }

    public completeprofilePage clickDataDate() throws Exception, InterruptedException {
        waitAndClickElement(dobDate);
        return new completeprofilePage();
    }

    public completeprofilePage clickDataYear() throws IOException, InterruptedException {
        waitAndClickElement(dobYear);
        return new completeprofilePage();
    }

    //skin type
    public completeprofilePage clickSkinType_Dry() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(skintype_dry);
//        waitAndClickElement(SkinType_Dry);
        return new completeprofilePage();
    }

    //skin tone
    public completeprofilePage clickSkinTone_Medium() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(skintone_medium);
        return new completeprofilePage();
    }

    //skin undertone
    public completeprofilePage clickSkinUnderTone_Warm() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(skinundertone_warm);
        return new completeprofilePage();
    }

    //hair_type
    public completeprofilePage clickHairType_Curly() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(hairtype_curly);
        return new completeprofilePage();
    }

    //hair_color
    public completeprofilePage clickHairColor_Yes() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(haircolor_yes);
        return new completeprofilePage();
    }

    //hijab
    public completeprofilePage clickHijaber_Yes() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(hijab_yes);
        return new completeprofilePage();
    }

    //skin concern
    public completeprofilePage clicSkinCon_Blackheads() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(skincon_bwheads);
        return new completeprofilePage();
    }

    //body_concerns
    public completeprofilePage clickBodyCon_Dryness() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(bodycon_dryness);
        return new completeprofilePage();
    }

    //hair_concerns
    public completeprofilePage clickHairCon_Dryness() throws IOException, InterruptedException {
        waitAndClickElementsUsingByLocator(haircon_dryness);
        return new completeprofilePage();
    }

    //fill this later
    // --- skin type
    public completeprofilePage clickFillThisLaterSkinCon() throws Exception {
        waitAndClickElementsUsingByLocator(fillthislaterSkinCon);
        return new completeprofilePage();
    }

    // --- skin concern
    public completeprofilePage clickFillThisLaterBodyCon() throws Exception {
        waitAndClickElementsUsingByLocator(fillthislaterBodyCon);
        return new completeprofilePage();
    }

    // --- hair concern
    public completeprofilePage clickFillThisLaterHairCon() throws Exception {
        waitAndClickElementsUsingByLocator(fillthislaterHairCon);
        return new completeprofilePage();
    }


    //summary
    public completeprofilePage clickAddPhoto() throws IOException, InterruptedException {
        waitAndClickElement(addPhoto);
        return new completeprofilePage();
    }

    public completeprofilePage fillFullname() throws Exception {
        sendKeysToWebElement(fieldFullname,"akun test", Keys.ENTER);
        return new completeprofilePage();
    }

    public completeprofilePage fillNoTelephone() throws Exception {
        sendKeysToWebElement(fieldPhoneNo,"0812849150051", Keys.ENTER);
        return new completeprofilePage();
    }

    public completeprofilePage fillBio() throws Exception {

        WebElement focusfillbio = fieldBio; //xpath megamenu nya
        Actions onfocusbio = new Actions(driver);
        onfocusbio.moveToElement(focusfillbio).click();
        onfocusbio.sendKeys("bio akun test", Keys.ENTER);
        onfocusbio.build().perform();

        return new completeprofilePage();
    }

    public completeprofilePage chooseLocation() throws Exception {

//        waitAndClickElement(optionCity);
//        WaitUntilWebElementIsVisible(listCity);
//        actionMoveAndClick(listCity);
        //coba tambahin keys.enter klo g bisa

        WebElement focusfillloc = optionCity; //xpath megamenu nya
        Actions onfocusfilloc = new Actions(driver);
        onfocusfilloc.moveToElement(focusfillloc).click();
        onfocusfilloc.sendKeys("a", Keys.ENTER);
        onfocusfilloc.build().perform();

        return new completeprofilePage();
    }

    public completeprofilePage chooseFavBrand() throws Exception {

        WebElement focusfavbrand= optionFavBrand; //xpath megamenu nya
        Actions onfocusfavbrand = new Actions(driver);
        onfocusfavbrand.moveToElement(focusfavbrand).click();
        onfocusfavbrand.sendKeys("warda", Keys.ENTER);
        onfocusfavbrand.build().perform();

        return new completeprofilePage();
    }

    public completeprofilePage clickFinishBtn() throws Exception {

        scrollToElementByWebElementLocator(btnFinish);

//        scrollToElementByWebElementLocator(btnFinish);

        jsClick(btnFinish);
        return new completeprofilePage();
    }

}
