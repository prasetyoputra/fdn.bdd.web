package java.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import pageObjects.basePage;

public class grouplisttalkPage extends basePage {

    public @FindBy(xpath ="//select[@class='groups-select']") WebElement sortoptiongrouplist;

    public @FindBy(xpath ="//div[@class='groups-list']//div[1]//div[1]//button[1]") WebElement joinfirstgroup;
    public @FindBy(xpath ="//div[@class='groups-list']//div[1]//div[1]//button[1]") WebElement joinsecondgroup;
    public @FindBy(xpath ="//div[@class='groups-list']//div[1]//div[1]//button[1]") WebElement jointhirdgroup;

    public @FindBy(xpath ="//h2[contains(text(),\"Men's Grooming\")]") WebElement clickgroupmensgrooming;
    public @FindBy(xpath ="//h2[contains(text(),'Textured Skin')]") WebElement clickgrouptexturedskin;
    public @FindBy(xpath ="//h2[contains(text(),'Combination Skin')]") WebElement clickgroupcombinationskin;


    public grouplisttalkPage() throws IOException {
        super();
    }

    public grouplisttalkPage clickJoinGroupAtGroupList() throws Exception {
        waitAndClickElement(joinfirstgroup);
        return new grouplisttalkPage();
    }

    public grouplisttalkPage clickGroup() throws Exception {
        waitAndClickElement(clickgroupmensgrooming);
        return new grouplisttalkPage();
    }



}
