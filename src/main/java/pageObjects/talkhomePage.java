package java.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import pageObjects.basePage;

public class talkhomePage extends basePage {

    By grouptab=By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[1]/a[2]");

    public talkhomePage() throws IOException {
        super();
    }


    public talkhomePage getCurentUrlTalk() throws IOException {

        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("talk.femaledaily"));

        return new talkhomePage();
    }

    public talkhomePage clickGroubTab() throws IOException, InterruptedException {

        waitAndClickElementsUsingByLocator(grouptab);

        return new talkhomePage();
    }
}
