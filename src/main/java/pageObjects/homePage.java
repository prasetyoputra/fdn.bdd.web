package pageObjects;

import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import pageObjects.basePage;

public class homePage extends basePage {

    public @FindBy(xpath ="//*[@id=\'button-login\']") WebElement loginHeader;
    public @FindBy(xpath ="//*[@id=\'__next\']/div/div/div[1]/div/div[1]/div[2]/div/span") WebElement usernameHeader;

    public @FindBy(xpath ="//span[@id='icon-burger-menu']") WebElement hamburgerMenu;
    public @FindBy(xpath ="//a[contains(text(),'Reviews')]") WebElement reviewsMenu;
    public @FindBy(xpath ="//a[contains(text(),'Editorial')]") WebElement editorialMenu;
    public @FindBy(xpath ="//a[contains(text(),'Forum')]") WebElement forumMenu;

    public @FindBy(xpath ="//a[contains(text(),'SkinCare')]") WebElement skincareMenu;
    public @FindBy(xpath ="//a[contains(text(),'Make Up')]") WebElement makeupMenu;
    public @FindBy(xpath ="//a[contains(text(),'Body')]") WebElement bodyMenu;
    public @FindBy(xpath ="//a[contains(text(),'Hair')]") WebElement hairMenu;
    public @FindBy(xpath ="//a[contains(text(),'Fragrance')]") WebElement fragranceMenu;

    public homePage() throws IOException {
        super();
    }

    public homePage clickOnLoginHeader() throws IOException, InterruptedException {
        waitAndClickElement(loginHeader);
        return new homePage();
    }

    public homePage clickOnHamburgerMenu() throws IOException, InterruptedException {
        waitAndClickElement(hamburgerMenu);
        return new homePage();
    }

    public homePage clickMenuReviews() throws IOException, InterruptedException {
        waitAndClickElement(reviewsMenu);
        return new homePage();
    }

    public homePage clickSkincareCat() throws IOException, InterruptedException {
        waitAndClickElement(skincareMenu);
        return new homePage();
    }

    public String checkLoggedIn()  throws IOException {

        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("femaledaily"));

        WaitUntilWebElementIsVisible(usernameHeader);
        String elementLoginHeader = usernameHeader.getText();
//        Assert.assertEquals("hi,testqa", voucherCodeValue.toLowerCase().replaceAll("[ ()0-9]", ""));
        Assert.assertNotNull(elementLoginHeader);
        return elementLoginHeader;
    }

    public homePage checkDirectHomepage() throws Throwable {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("femaledaily"));
        return new homePage();
    }

}
