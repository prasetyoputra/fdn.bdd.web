package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.IOException;
import pageObjects.basePage;

public class productdetailPage extends basePage {

    public @FindBy(xpath ="//button[contains(text(),'add review')]") WebElement addreviewButton;


    /* form add review */
    By reviewbox=By.xpath("//*[@id='form-add-review']/div[1]/textarea");
    public @FindBy(xpath ="//i[text()='2']") WebElement rating2;
    public @FindBy(xpath ="//i[text()='3']") WebElement rating3;
    public @FindBy(xpath ="//i[text()='4']") WebElement rating4;
    public @FindBy(xpath ="//i[text()='5']") WebElement rating5;

    public @FindBy(xpath ="//button[contains(text(),'value for money')]") WebElement valueformoneyOption;
    public @FindBy(xpath ="//button[contains(text(),'just right')]") WebElement justrightOption;
    public @FindBy(xpath ="//button[contains(text(),'expensive')]") WebElement expensiveOption;

    public @FindBy(xpath ="//button[contains(text(),'can be improved')]") WebElement canimprovedOption;
    public @FindBy(xpath ="//button[contains(text(),'okay')]") WebElement okayOption;
    public @FindBy(xpath ="//button[contains(text(),'good')]") WebElement goodOption;
    public @FindBy(xpath ="//button[contains(text(),'perfect')]") WebElement perfectOption;

    public @FindBy(xpath ="//button[contains(text(),'Yes')]") WebElement yestorepurchaseOption;
    public @FindBy(xpath ="//button[contains(text(),'No')]") WebElement notorepurchaseOption;

    public @FindBy(xpath ="//input[@value='SUBMIT REVIEW']") WebElement submitreviewButton;


    public productdetailPage() throws IOException {
        super();
    }

    public productdetailPage clickAddReviewButton() throws Exception {
        waitAndClickElement(addreviewButton);
        return new productdetailPage();
    }

    public productdetailPage fillReviewDesc() throws Exception {
        sendKeysToWebByElement(reviewbox,"test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test", Keys.ENTER);
        return new productdetailPage();
    }

    public productdetailPage chooseOverallRating() throws Exception {
        waitAndClickElement(rating3);
        return new productdetailPage();
    }

    public productdetailPage chooseProductPrice() throws Exception {
        waitAndClickElement(justrightOption);
        return new productdetailPage();
    }

    public productdetailPage choosePackageQuality() throws Exception {
        waitAndClickElement(goodOption);
        return new productdetailPage();
    }

    public productdetailPage chooseRepurchase() throws Exception {
        waitAndClickElement(yestorepurchaseOption);
        return new productdetailPage();
    }

    public productdetailPage checkReviewOnMostTop() throws Exception {

        (new WebDriverWait(driver, 50)).until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[@id='name-brand']")));

        String descrevBox = driver.findElement(By.xpath("//*[@id='prodlist-review-cover']/div[1]/div/div/div[2]/div[1]/p")).getText().toString();
        Assert.assertEquals(descrevBox, "test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test");
        return new productdetailPage();
    }

    public productdetailPage clickSubmitReviewButton() throws Exception {
        waitAndClickElement(submitreviewButton);
        return new productdetailPage();
    }

}
