package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.io.IOException;
import pageObjects.basePage;

public class registerPage extends basePage {

    public @FindBy(css = "#id-email") WebElement emailField;
    public @FindBy(css = "#id-username") WebElement usernameField;
    public @FindBy(css = "#id-password") WebElement passwordField;
    public @FindBy(css = "#id-password-confirm") WebElement confirmPasswordField;

    public @FindBy(xpath = "//*[@id=\'__next\']/div/div/div[2]/div/form/div[6]/i") WebElement boxAgreeTerms;
    public @FindBy(linkText = "Terms & Conditions") WebElement linkTC;
    public @FindBy(linkText = "Privacy Policy") WebElement linkPP;

    public @FindBy(xpath = "//*[@id=\'__next\']/div/div/div[2]/div/form/div[7]/div/input") WebElement buttonCreateAccount;
    public @FindBy(css = "input[value='Login with Facebook']") WebElement buttonLoginFb;
    public @FindBy(css = "input[value='Login']") WebElement buttonLogin;

//    public @FindBy(xpath = "//*[@id=\"__next\"]/div/div/div[2]/div/form/div[1]/p") WebElement elementErrorMsg;
    By elementErrorMsg=By.xpath("//*[@id=\"__next\"]/div/div/div[2]/div/form/div[1]/p");


    public registerPage() throws IOException {
        super();
    }

    /*  wait element */
    public registerPage waitregisterpageload() throws Exception {
        WaitUntilWebElementIsVisible(emailField);
        WaitUntilWebElementIsVisible(usernameField);
        WaitUntilWebElementIsVisible(passwordField);
        WaitUntilWebElementIsVisible(confirmPasswordField);
        return new registerPage();
    }

    /*  click sendkeys element */
    public registerPage inputEmail(String email) throws Exception {
        sendKeysToWebElement(emailField, email, Keys.ENTER);
        return new registerPage();
    }

    public registerPage inputUsername(String username) throws Exception {
        sendKeysToWebElement(usernameField, username, Keys.ENTER);
        return new registerPage();
    }

    public registerPage inputPassword(String password) throws Exception {
        sendKeysToWebElement(passwordField, password, Keys.ENTER);
        return new registerPage();
    }

    public registerPage inputWrongConfirmPassword(String wrongpassword) throws Exception {
        sendKeysToWebElement(confirmPasswordField, wrongpassword, Keys.ENTER);
        return new registerPage();
    }

    public registerPage inputConfirmPassword(String passwordagain) throws Exception {
        sendKeysToWebElement(confirmPasswordField, passwordagain, Keys.ENTER);
        return new registerPage();
    }

    public registerPage checkboxAgree() throws Exception {
        waitAndClickElement(boxAgreeTerms);
        return new registerPage();
    }

    public registerPage clickButtonCreateAccount() throws Exception {
        waitAndClickElement(buttonCreateAccount);
        return new registerPage();
    }

    public registerPage clickButtonLoginFacebook() throws Exception {
        waitAndClickElement(buttonLoginFb);
        return new registerPage();
    }

    public registerPage clickTermsCond() throws Exception {
        waitAndClickElement(linkTC);
        return new registerPage();
    }

    public registerPage clickPrivacyPolicy() throws Exception {
        waitAndClickElement(linkPP);
        return new registerPage();
    }

    public registerPage clickButtonLogin() throws Exception {
        waitAndClickElement(buttonLogin);
        return new registerPage();
    }

    /* error message */

    public registerPage getMsgEmailRegistered() throws Exception {

        WaitUntilWebElementIsVisibleUsingByLocator(elementErrorMsg);

        String msgEmailRegistered = getMesssageError(elementErrorMsg);
        Assert.assertEquals(msgEmailRegistered, "Email already registered");

        return new registerPage();
    }

    public registerPage getMsgPasswordDoesntMatch() throws Exception {

        WaitUntilWebElementIsVisibleUsingByLocator(elementErrorMsg);

        String msgPasswordDoesntMatch = getMesssageError(elementErrorMsg);
        Assert.assertEquals(msgPasswordDoesntMatch, "Your confirmation password doesn't match your password");

        return new registerPage();
    }

    public registerPage getMsgUsernameRegistered() throws Exception {

        WaitUntilWebElementIsVisibleUsingByLocator(elementErrorMsg);

        String msgUsernameTaken = getMesssageError(elementErrorMsg);
        Assert.assertEquals(msgUsernameTaken, "Username taken");

        return new registerPage();
    }

    public registerPage getMsgInvFormatEmail() throws Exception {

        WaitUntilWebElementIsVisibleUsingByLocator(elementErrorMsg);

        String msgUsernameTaken = getMesssageError(elementErrorMsg);
        Assert.assertEquals(msgUsernameTaken, "An email address must contain a single @ and a single dot");

        return new registerPage();
    }

    public registerPage getMsgUsernamelessOrThan() throws Exception {

        WaitUntilWebElementIsVisibleUsingByLocator(elementErrorMsg);

        String msgUsernameTaken = getMesssageError(elementErrorMsg);
        Assert.assertEquals(msgUsernameTaken, "Username should be 3-20 characters in length");

        return new registerPage();
    }

}
