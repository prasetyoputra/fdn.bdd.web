package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import pageObjects.basePage;

public class addproductPage extends basePage {

    public @FindBy(xpath ="//input[@value='Yes, I’m sure']") WebElement yesimsureButton;
    public @FindBy(xpath ="//button[@class='jsx-4293741256 modal-add-product-btn-grey']") WebElement cancelButton;


    public @FindBy(xpath ="//input[@id='addproduct-image-url']") WebElement imageurlField;
    public @FindBy(xpath ="//button[@id='addproduct-button-show-image']") WebElement showimageurlButton;
    public @FindBy(xpath ="//div[@id='react-select-2--value']") WebElement brandnameOption;
    public @FindBy(id ="react-select-3--value") WebElement catnameOption;
    public @FindBy(id ="react-select-4--value") WebElement subcatnameOption;
    public @FindBy(id ="addproduct-input-product-name") WebElement productnameOption;
    public @FindBy(id ="addproduct-input-product-shade") WebElement productshadeOption;

    public @FindBy(xpath ="//textarea[@id='review-field']") WebElement reviewField;
    public @FindBy(xpath ="//input[@id='number-format']") WebElement retailpriceField;
    public @FindBy(xpath ="//textarea[@id='description-field']") WebElement productdescField;
    public @FindBy(xpath ="//div[@class='Select-placeholder']") WebElement producttagsField;

    //step1
    By uploadphotoButton = By.id("addproduct-button-upload");
    By croparea = By.xpath("//div[@class='ReactCrop__crop-selection']");
    By cropButton = By.xpath("//button[contains(text(),'crop')]");
    By nextstep1Button = By.id("addproduct-button-next");

    //step2
    By productsubcat2Option = By.id("react-select-5--value");
//    By productnameOption = By.id("addproduct-input-product-name");
//    By productshadeOption = By.id("addproduct-input-product-shade");
    By nextstep2Button = By.id("addproduct-button-submit");

    //step3
    By rating1=By.id("star-1");
    By rating2=By.id("star-2");
    By rating3=By.id("star-3");
    By rating4=By.id("star-4");
    By rating5=By.id("star-5");
    By productpricevalurformoney=By.id("rating-price-1");
    By productpricejsutright=By.id("rating-price-2");
    By productpricexpensive=By.id("rating-price-3");
    By packagequalitypoor=By.id("rating-quality-1");
    By packagequalityimproved=By.id("rating-quality-2");
    By packagequalityokay=By.id("rating-quality-3");
    By packagequalitygood=By.id("rating-quality-4");
    By packagequalityperfect=By.id("rating-quality-5");
    By repurchaseyes=By.id("repurchase-yes");
    By repurchaseno=By.id("repurchase-no");
    By writereview=By.id("review-field");
    By nextstep3Button=By.id("product-rating-button-submit");

    //step4
    By matauang=By.id("react-select-8--value");
    By descr=By.id("description-field");
    By producttags=By.xpath("//*[@id='top-page']/div[5]/div[2]/form/div[1]/div[5]");
    By submit=By.id("product-info-button-submit");

    //header label
    By headerstep1=By.xpath("//*[@id='top-page']/div[2]/div[1]/span");
    By headerstep2=By.xpath("//*[@id='top-page']/div[3]/div[1]/span");
    By headerstep3=By.xpath("//*[@id='top-page']/div[4]/div[1]/span");

    public addproductPage() throws IOException {
        super();
    }

    public addproductPage clickImSure() throws Exception {
        waitAndClickElement(yesimsureButton);
        return new addproductPage();
    }

    public addproductPage getCurentUrlAddProduct() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/add-product"));
        return new addproductPage();
    }

    public addproductPage clickUploadPhoto() throws Exception {

        waitAndClickElementsUsingByLocator(uploadphotoButton);

        File file1 = new File("/Users/mac/product-test.jpg");
        StringSelection stringSelection1= new StringSelection(file1.getAbsolutePath());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection1, null);

        Robot robot1 = new Robot();

        // Cmd + Tab is needed since it launches a Java app and the browser looses focus

        robot1.keyPress(KeyEvent.VK_META);
        robot1.keyPress(KeyEvent.VK_TAB);
        robot1.keyRelease(KeyEvent.VK_META);
        robot1.keyRelease(KeyEvent.VK_TAB);
        robot1.delay(800);
        //Open Goto window
        robot1.keyPress(KeyEvent.VK_META);
        robot1.keyPress(KeyEvent.VK_SHIFT);
        robot1.keyPress(KeyEvent.VK_G);
        robot1.keyRelease(KeyEvent.VK_META);
        robot1.keyRelease(KeyEvent.VK_SHIFT);
        robot1.keyRelease(KeyEvent.VK_G);
        //Paste the clipboard value
        robot1.keyPress(KeyEvent.VK_META);
        robot1.keyPress(KeyEvent.VK_V);
        robot1.keyRelease(KeyEvent.VK_META);
        robot1.keyRelease(KeyEvent.VK_V);
        //Press Enter key to close the Goto window and Upload window
        robot1.keyPress(KeyEvent.VK_ENTER);
        robot1.keyRelease(KeyEvent.VK_ENTER);
        robot1.delay(800);
        robot1.keyPress(KeyEvent.VK_ENTER);
        robot1.keyRelease(KeyEvent.VK_ENTER);
        Thread.sleep(5000);

        Actions crop = new Actions(driver);
        Thread.sleep(2000);

        WebElement trycrop = driver.findElementByCssSelector("#modal-crop-showed > div > div.ReactCrop.ReactCrop--fixed-aspect > img");

        //Move to the desired co-ordinates of the image element, In the code below I am staring from bottom left corner of the image
        crop.moveToElement(driver.findElement(croparea),0,0);

        //locate the co-ordinates of image you want to move by and perform the click   and hold which mimics the crop action
        crop.clickAndHold().moveByOffset(196,238).release().build().perform();

        waitAndClickElementsUsingByLocator(cropButton);

        return new addproductPage();
    }

    public addproductPage clickNextStep1() throws Exception {
        waitAndClickElementsUsingByLocator(nextstep1Button);
        return new addproductPage();
    }

    public addproductPage clickNextStep2() throws Exception {
        waitAndClickElementsUsingByLocator(nextstep2Button);
        return new addproductPage();
    }

    public addproductPage clickNextStep3() throws Exception {
        waitAndClickElementsUsingByLocator(nextstep3Button);
        return new addproductPage();
    }

    public addproductPage clickSubmit() throws Exception {
        waitAndClickElementsUsingByLocator(submit);
        return new addproductPage();
    }

    public addproductPage pasteUrlImages() throws Exception {
        sendKeysToWebElementWihoutEnter(imageurlField, "http://cdn.femaledaily.com/forum/assets/img/fd_logo_3.png");
        waitAndClickElement(showimageurlButton);
        Thread.sleep(3000);
        return new addproductPage();
    }

    public addproductPage clickShowUrlImages() throws Exception {
        waitAndClickElement(showimageurlButton);
        return new addproductPage();
    }

    public addproductPage selectBrandName() throws Exception {

        WebElement focusbrand= brandnameOption; //xpath megamenu nya
        Actions onfocusbrand = new Actions(driver);
        onfocusbrand.moveToElement(focusbrand).click();
        onfocusbrand.sendKeys("warda", Keys.ENTER);
        onfocusbrand.build().perform();

        return new addproductPage();
    }

    public addproductPage selectCategoryName() throws Exception {

        WebElement focusprodcat = catnameOption; //xpath megamenu nya
        Actions onfocusprodcat = new Actions(driver);
        onfocusprodcat.moveToElement(focusprodcat).click();
        onfocusprodcat.sendKeys("frag", Keys.ENTER);
        onfocusprodcat.build().perform();

        return new addproductPage();
    }

    public addproductPage selectSubCategoryName() throws Exception {

        WebElement focusProductSubCat= subcatnameOption; //xpath megamenu nya
        Actions onfocusProductSubCat = new Actions(driver);
        onfocusProductSubCat.moveToElement(focusProductSubCat).click();
        onfocusProductSubCat.sendKeys("edp", Keys.ENTER);
        onfocusProductSubCat.build().perform();

        return new addproductPage();
    }

    public addproductPage fillProductName(String productname) throws Exception {

        WebElement focusProductName= productnameOption; //xpath megamenu nya
        Actions onfocusProductName = new Actions(driver);
        onfocusProductName.moveToElement(focusProductName).click();
        onfocusProductName.sendKeys(productname);
        onfocusProductName.build().perform();

        return new addproductPage();
    }

    public addproductPage fillProductShade(String productshade) throws Exception {

        WebElement focusProductName= productshadeOption; //xpath megamenu nya
        Actions onfocusProductName = new Actions(driver);
        onfocusProductName.moveToElement(focusProductName).click();
        onfocusProductName.sendKeys(productshade);
        onfocusProductName.build().perform();

        return new addproductPage();
    }

    public addproductPage chooseOverallRating() throws Exception {
        waitAndClickElementsUsingByLocator(rating2);
        return new addproductPage();
    }

    public addproductPage chooseProductPrice() throws Exception {
        waitAndClickElementsUsingByLocator(productpricejsutright);
        return new addproductPage();
    }

    public addproductPage choosePackageQuality() throws Exception {
        waitAndClickElementsUsingByLocator(packagequalityokay);
        return new addproductPage();
    }

    public addproductPage chooseRepurchase() throws Exception {
        waitAndClickElementsUsingByLocator(repurchaseno);
        return new addproductPage();
    }

    public addproductPage fillWriteReview() throws Exception {
        sendKeysToWebElement(reviewField, "descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest descriptiontest" , Keys.ENTER);
        return new addproductPage();
    }

    public addproductPage fillRetailPrice() throws Exception {
        sendKeysToWebElement(retailpriceField, "50000", Keys.ENTER);
        return new addproductPage();
    }

    public addproductPage fillProductDesc() throws Exception {
        sendKeysToWebElement(productdescField, "test product desc", Keys.ENTER);
        return new addproductPage();
    }

    public addproductPage fillProductTags() throws Exception {

        WebElement focusproducttags= producttagsField; //xpath megamenu nya
        Actions onfocusproducttags = new Actions(driver);
        onfocusproducttags.moveToElement(focusproducttags).click();
        onfocusproducttags.sendKeys("warda", Keys.ENTER);
        onfocusproducttags.build().perform();

        return new addproductPage();
    }

    public addproductPage getCurentUrlSubmittedProduct() throws IOException {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/products/fragrance/edp/wardah/testname"));
        return new addproductPage();
    }



}
