package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import pageObjects.basePage;

public class reviewsPage extends basePage {

    public @FindBy(xpath ="//div[@class='gbheader-add-area']") WebElement addProductReviewHeaderElement;
    public @FindBy(xpath ="//a[contains(text(),'Add Product')]") WebElement addProductElement;
    public @FindBy(xpath ="//a[contains(text(),'Add Review')]") WebElement addReviewElement;

    public @FindBy(xpath ="//p[contains(text(),'Aloe Vera 92% Soothing Gel')]") WebElement firstPopularProduct;

    // write a review tab
    public @FindBy(xpath ="//textarea[@placeholder='Add your review (at least 200 characters long). Tip : A good review includes your experience while using the product. wether it was effective or not. how long did it last. etc.']") WebElement addreviewBox;
    public @FindBy(xpath ="//i[text()='3']") WebElement rating3Option;
    public @FindBy(xpath ="//button[contains(text(),'just right')]") WebElement productpricerightOption;
    public @FindBy(xpath ="//button[contains(text(),'good')]") WebElement packagequalitygoodOption;
    public @FindBy(xpath ="//button[contains(text(),'Yes')]") WebElement repurchaseyesOption;

    public @FindBy(xpath ="//input[@value='SUBMIT REVIEW']") WebElement submitreviewButton;


    public reviewsPage() throws IOException {
        super();
    }

    // getCurrentURL
    public reviewsPage getCurentUrlReviews() throws IOException {

        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("reviews.femaledaily"));

        return new reviewsPage();
    }

    public reviewsPage hoverClickAddProductElement() throws Exception {

        Actions act = new Actions(driver);
        act.moveToElement(addProductReviewHeaderElement).perform();

        actionMoveAndClick(addProductElement);

        return new reviewsPage();
    }

    public reviewsPage hoverClickAddReviewElement() throws Exception {

        Actions act = new Actions(driver);
        act.moveToElement(addProductReviewHeaderElement).perform();

        actionMoveAndClick(addReviewElement);

        return new reviewsPage();
    }

    public reviewsPage getCurrentUrlAddReviewPage() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/review/add"));
        //WaitUntilWebElementIsVisible(firstPopularProduct);
        return new reviewsPage();
    }

    public reviewsPage clickOneOfPopularProducts() throws Exception {
        waitAndClickElement(firstPopularProduct);
        return new reviewsPage();
    }

    public reviewsPage checkIfUserOnReviewTab() throws Exception {
        WaitUntilWebElementIsVisible(addreviewBox);
        return new reviewsPage();
    }

    public reviewsPage chooseOverallRating() throws Exception {
        waitAndClickElement(addreviewBox);
        return new reviewsPage();
    }

    public reviewsPage chooseProductPrice() throws Exception {
        waitAndClickElement(productpricerightOption);
        return new reviewsPage();
    }

    public reviewsPage choosePackageQuality() throws Exception {
        waitAndClickElement(packagequalitygoodOption);
        return new reviewsPage();
    }

    public reviewsPage chooseRepurchaseYes() throws Exception {
        waitAndClickElement(repurchaseyesOption);
        return new reviewsPage();
    }

    public reviewsPage clickSubmitReview() throws Exception {
        scrollToElementByWebElementLocator(submitreviewButton);
        waitAndClickElement(submitreviewButton);
        return new reviewsPage();
    }


}
