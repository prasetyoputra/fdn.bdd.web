package java.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import pageObjects.basePage;

public class groupdetailPage extends basePage {

    public @FindBy(xpath ="//button[contains(text(),'ADD TOPIC')]") WebElement clickaddtopic;


    public groupdetailPage() throws IOException {
        super();
    }

    public groupdetailPage clickAddTopic() throws Exception {
        waitAndClickElement(clickaddtopic);
        return new groupdetailPage();
    }
}
