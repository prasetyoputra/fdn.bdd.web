package pageObjects;

import cucumber.api.java.en.Then;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import pageObjects.basePage;

public class categorylandingPage extends basePage {

    public @FindBy(xpath ="//a[contains(text(),'Night Cream')]") WebElement nightcreamMenu;
    public @FindBy(xpath ="//a[@id='id_product_image_14598']") WebElement productPerfect3Dhadalabo;

    public @FindBy(xpath ="//button[contains(text(),'add review')]") WebElement addreviewButton;

    public categorylandingPage() throws IOException {
        super();
    }

    public categorylandingPage clickNightCreamMenu() throws IOException, InterruptedException {
        waitAndClickElement(nightcreamMenu);
        return new categorylandingPage();
    }
    public categorylandingPage getCurentUrlSkinLandingPage() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("/category/skincare"));
        return new categorylandingPage();
    }

    public categorylandingPage getCurentUrlProdListPage() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("brand=&order=popular&page=1"));
        return new categorylandingPage();
    }

    public categorylandingPage getCurentUrlProdDetail() throws Exception {
        (new WebDriverWait(driver, 50)).until(ExpectedConditions.urlContains("tab=reviews&cat=&cat_id=0&age_range=&skin_type=&skin_tone=&skin_undertone=&hair_texture=&hair_type=&order=newest&page=1"));
        return new categorylandingPage();
    }

    public categorylandingPage clickFirstProduct() throws Exception {
        waitAndClickElement(productPerfect3Dhadalabo);
        return new categorylandingPage();
    }

}
